//
// gen.h
//
// Copyright: (c) 2012 by Szymon Urbaś <szymon.urbas@aol.com>
//

#ifndef GEN_H
#define GEN_H

#include "nemo.h"

struct Node *genDeclaration(Type, char *, struct Node *, struct Node *);
struct Node *genAssignment(char *, struct Node *, struct Node *);
struct Node *genEmptyBlock(struct Node *, struct Node *);
       void  appendToBlock(struct Node *, struct Node *);
struct Node *genBinaryop(struct Node *, struct Node *, Binary, struct Node *);
struct Node *genUnaryop(struct Node *, Unary, struct Node *);
struct Node *genCall(struct Node *, struct ParamList *, int);
struct Node *genReturn(struct Node *);
struct Node *genWhile(struct Node *, struct Node *);
struct Node *genIf(struct Node *, struct Node *, struct Node *);
struct Node *genFor(struct Node *, struct Node *, struct Node *, struct Node *, struct Node *);
struct Node *genFuncDef(Type, char *, struct ArgList *, int);
struct Node *genExpByInt(int);
struct Node *genExpByFloat(float);
struct Node *genExpBySingleString(char *);
struct Node *genExpByDoubleString(char *, struct Node *);
struct Node *genExpByName(char *, struct Node *);
struct Node *genIter(char *, struct Node *, struct Node *);
struct Node *genNoop(void);

struct ArgList *genArgList(Type, char *, struct ArgList *, int);
struct ParamList *genParamList(struct Node *, struct ParamList *, int);

#endif // GEN_H
